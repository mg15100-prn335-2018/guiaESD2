
// EJERCICIO 2
//Elmer Antonio Martir Garcia - MG15100

#include <stdio.h>

int TAM;
void CargarVector(int arr[TAM], int TAM);
void MostrarVector(int arr[TAM], int TAM);
void EliminarElemento(int arr[TAM], int *TAM, int elem);

int main(){
printf("¿Cuantos elementos desea ingresar en el vector? "); 
scanf("%d", &TAM);
    int option;
    int arr[TAM];
   // int cantidad;
    int elem;

    do{
        printf("\n--------Menu--------\n");
        printf("\n");
        printf("0: Salir\n");
        printf("1: LLenar vector\n");
        printf("2: Mostar vector\n");
        printf("3: Eliminar posicion del vector\n");
        scanf("%d", &option);
        switch(option){
            case 1: CargarVector(arr, TAM); break;
            case 2: MostrarVector(arr, TAM); break;
            case 3: printf("Ingrese elemento a eliminar: "); scanf("%d", &elem);
                    EliminarElemento(arr, &TAM, elem); break;
        }
    } while(option!=0);

    return 0;
}

void CargarVector(int arr[TAM], int TAM){
    int i;

    for(i= 0; i<TAM; i+=1){
        printf("Ingrese elemento: "); scanf("%d", &arr[i]);
    }
}

void MostrarVector(int arr[TAM], int TAM){
    int i;
    for(i= 0; i<TAM; i+=1){
        printf("Elemento[%d]= %d\n", i, arr[i]);
    }
}

void EliminarElemento(int arr[TAM], int *TAM, int elem){
    int i;

    for(i= elem; i<*TAM-1; i+=1){
        arr[i]= arr[i+1];
    }
    *TAM-=1;
}
